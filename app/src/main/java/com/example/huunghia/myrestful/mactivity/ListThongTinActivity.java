package com.example.huunghia.myrestful.mactivity;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AbsListView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.example.huunghia.myrestful.R;
import com.example.huunghia.myrestful.api.XPSApiEndpoint;
import com.example.huunghia.myrestful.api.XPSClient;
import com.example.huunghia.myrestful.api.XPSExecParam;
import com.example.huunghia.myrestful.model.Info;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

import retrofit2.Call;

public class ListThongTinActivity extends AppCompatActivity {

    private ListView listView;
    private int firstVisibleItem, visibleItemCount,totalItemCount;
    private int offset, number;
    private View footerView;

    ArrayAdapter<String> adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_thong_tin);

        offset = 0;
        number = 0;

        footerView = ((LayoutInflater) getApplicationContext()
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE))
                .inflate(R.layout.footer_view, null, false);

        listView = findViewById(R.id.listView);
        adapter = new ArrayAdapter<>(ListThongTinActivity.this,
                android.R.layout.simple_list_item_1, new ArrayList<String>());
        listView.setAdapter(adapter);

        showInfo();

        listView.setOnScrollListener(new AbsListView.OnScrollListener() {

            @Override
            public void onScrollStateChanged(AbsListView absListView, int scrollState) {
                final int lastItem = firstVisibleItem + visibleItemCount;
                if (lastItem == totalItemCount && scrollState == SCROLL_STATE_IDLE){
                    showInfo();
                }
            }

            @Override
            public void onScroll(AbsListView absListView, int firstVisibleItemm,
                                 int visibleItemCountt, int totalItemCountt) {
                firstVisibleItem = firstVisibleItemm;
                visibleItemCount = visibleItemCountt;
                totalItemCount = totalItemCountt;
            }
        });
    }

    private void showInfo(){
        new AsyncTask<Void, Void, ArrayList<Info>>(){

            @Override
            protected ArrayList<Info> doInBackground(Void... voids) {

                ArrayList args00 = new ArrayList();
                args00.add("mobile");
                args00.add("!=");
                args00.add(false);


                ArrayList args0 = new ArrayList();
                args0.add(args00);

                String[] args1 = new String[]{"name", "email", "phone", "mobile"};

                ArrayList args = new ArrayList();
                args.add(args0);
                args.add(args1);
                args.add(offset);
                args.add(20);

                String method = "search_read";
                String model = "res.partner";

                XPSApiEndpoint xps = XPSClient.getXPSClient(ListThongTinActivity.this);
                XPSExecParam param = new XPSExecParam(model, method, args, new Object());
                Call<String> call = xps.loadInfo(param);

                String response = null;
                ArrayList<Info> infos = new ArrayList<>();

                try {
                    response = call.execute().body();

                    JSONObject root = new JSONObject(response);
                    JSONArray arrResult = root.getJSONArray("result");

                    int totalResult = arrResult.length();

                    for (int i = 0; i < totalResult; i++){
                        JSONObject resultObj = arrResult.getJSONObject(i);
                        String mobile = resultObj.getString("mobile");
                        String email = resultObj.getString("email");
                        int id = resultObj.getInt("id");
                        String phone = resultObj.getString("phone");
                        String name = resultObj.getString("name");

                        Info info = new Info(mobile, email, id, phone, name);
                        infos.add(info);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                return infos;
            }

            @Override
            protected void onPostExecute(ArrayList<Info> results) {
                super.onPostExecute(results);

                ArrayList<String> arrStr = new ArrayList<>();

                Iterator<Info> iterator = results.iterator();
                while (iterator.hasNext()){
                    Info info = iterator.next();
                    String email = info.getEmail();
                    if (email.equals("false")) info.setEmail("không có email");
                    String str = info.toString();
                    arrStr.add(str);
                }

                listView.addFooterView(footerView);
                SystemClock.sleep(500);
                adapter.addAll(arrStr);
                adapter.notifyDataSetChanged();

                number += 2;
                offset = (number - 1)*20;
            }
        }.execute();
    }

}
