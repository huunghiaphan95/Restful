package com.example.huunghia.myrestful.api;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface XPSApiEndpoint {

    @POST("/web/session/authenticate")
    Call<String> authenticate(@Body XPSCredsParam param);

    @POST("/web/session/get_session_info")
    Call<String> sessionInfo(@Body XPSEmptyParam param);

    @POST("/web/session/destroy")
    Call<String> logout(@Body XPSEmptyParam param);

    @POST("/web/dataset/call_kw")
    Call<String> loadImg(@Body XPSExecParam param);

    @POST("/web/dataset/call_kw")
    Call<String> loadInfo(@Body XPSExecParam param);

}
