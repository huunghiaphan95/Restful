package com.example.huunghia.myrestful.api;

import java.util.Collection;

public class XPSExecParam {
    public XPSExecParamDetail params;

    public XPSExecParam(String model, String method, Collection args, Object kwargs) {
        this.params = new XPSExecParamDetail(model, method, args, kwargs);
    }

    /**
     * Created by Admin on 9/1/2017.
     */

    public static class XPSExecParamDetail {
        public final String model;
        public final String method;
        public final Collection args;
        public final Object kwargs;

        public XPSExecParamDetail(String model, String method, Collection args, Object kwargs) {
            this.model = model;
            this.method = method;
            this.args = args;
            this.kwargs = kwargs;
        }
    }
}