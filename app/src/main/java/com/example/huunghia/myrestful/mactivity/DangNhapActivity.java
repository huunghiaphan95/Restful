package com.example.huunghia.myrestful.mactivity;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.huunghia.myrestful.R;
import com.example.huunghia.myrestful.api.XPSApiEndpoint;
import com.example.huunghia.myrestful.api.XPSClient;
import com.example.huunghia.myrestful.model.Result;
import com.example.huunghia.myrestful.api.XPSCredsParam;
import com.example.huunghia.myrestful.api.XPSEmptyParam;
import com.example.huunghia.myrestful.storage.SharedPrefManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import retrofit2.Call;

public class DangNhapActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText editTextLogin, editTextPassword;
    private Button button;

    private String db, login, password;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dang_nhap);

        anhxa();
        button.setOnClickListener(this);

    }

    @Override
    protected void onStart() {
        super.onStart();
        if (SharedPrefManager.getInstance(DangNhapActivity.this).checkSessionID())
            loginAgain();
    }

    @Override
    public void onClick(View view) {
        login();
    }

    private void anhxa() {
        editTextLogin       = findViewById(R.id.editTextLogin);
        editTextPassword    = findViewById(R.id.editTextPassword);
        button              = findViewById(R.id.button);
    }

    private void login(){
        db = "hhd_e300_vn_test01";
        login = editTextLogin.getText().toString();
        password = editTextPassword.getText().toString();

        if (login.length() > 0 && password.length() > 0) {
            new AsyncTask<Void, Void, Result>() {
                @Override
                protected Result doInBackground(Void... voids) {
                    Result result = new Result();

                    XPSApiEndpoint xps = XPSClient.getXPSClient(DangNhapActivity.this);
                    Call<String> call = xps.authenticate(new XPSCredsParam
                            (db, login, password));
                    String response = null;
                    try {
                        response = call.execute().body();

                        JSONObject root = new JSONObject(response);
                        JSONObject resultObj = root.getJSONObject("result");

                        String username = resultObj.getString("username");
                        String sessionId = resultObj.getString("session_id");

                        result.setUsername(username);
                        result.setSessionId(sessionId);

                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    return result;
                }

                @Override
                protected void onPostExecute(Result s) {
                    super.onPostExecute(s);

                    editTextLogin.setText("");
                    editTextPassword.setText("");

                    if (s.getUsername() == null) {
                        Toast.makeText(DangNhapActivity.this,
                                "sai ten dang nhap hoac mat khau",
                                Toast.LENGTH_SHORT).show();
                    } else {
                        Intent intent = new Intent
                                (DangNhapActivity.this, ThongTinTaiKhoanActivity.class);
                        intent.putExtra("name", s.getUsername());
                        startActivity(intent);

                        SharedPrefManager.getInstance(DangNhapActivity.this).saveSessionId(s);

                    }
                }
            }.execute();
        } else
            Toast.makeText(DangNhapActivity.this, "nhap day du ten dang nhap va mat khau",
                    Toast.LENGTH_SHORT).show();

    }

    private void loginAgain(){
        new AsyncTask<Void, Void, Result>() {
            @Override
            protected Result doInBackground(Void... voids) {
                Result result = new Result();

                XPSApiEndpoint xps = XPSClient.getXPSClient(DangNhapActivity.this);
                Call<String> call = xps.sessionInfo(new XPSEmptyParam());
                String response = null;
                try {
                    response = call.execute().body();

                    JSONObject root = new JSONObject(response);
                    JSONObject resultObj = root.getJSONObject("result");

                    String username = resultObj.getString("username");
                    result.setUsername(username);

                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                return result;
            }

            @Override
            protected void onPostExecute(Result s) {
                super.onPostExecute(s);
                if (s.getUsername() == null){

                    Toast.makeText(DangNhapActivity.this,
                            "nhap day du ten dang nhap va mat khau",
                            Toast.LENGTH_SHORT).show();
                }else {
                    Intent intent = new Intent
                            (DangNhapActivity.this, ThongTinTaiKhoanActivity.class);
                    intent.putExtra("name", s.getUsername());
                    startActivity(intent);
                }
            }
        }.execute();
    }
}
