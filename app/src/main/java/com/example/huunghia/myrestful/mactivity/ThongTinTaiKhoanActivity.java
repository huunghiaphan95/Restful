package com.example.huunghia.myrestful.mactivity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.huunghia.myrestful.R;
import com.example.huunghia.myrestful.api.XPSApiEndpoint;
import com.example.huunghia.myrestful.api.XPSClient;
import com.example.huunghia.myrestful.model.Result;
import com.example.huunghia.myrestful.api.XPSEmptyParam;
import com.example.huunghia.myrestful.api.XPSExecParam;
import com.example.huunghia.myrestful.storage.SharedPrefManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import retrofit2.Call;

public class ThongTinTaiKhoanActivity extends AppCompatActivity implements View.OnClickListener {

    private TextView textView;
    private ImageView imageView;
    private Button button, button2, button3;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_thong_tin_tai_khoan);

        anhxa();

        button.setOnClickListener(this);
        button2.setOnClickListener(this);
        button3.setOnClickListener(this);

        Intent intent = getIntent();

        textView.setText("Ten tai khoan: " + intent.getStringExtra("name"));


    }

    @Override
    public void onClick(View view) {

        if (view.getId() == R.id.button) logout();
        if (view.getId() == R.id.button2) loadImg();
        if (view.getId() == R.id.button3)
            startActivity(new Intent(ThongTinTaiKhoanActivity.this, ListThongTinActivity.class));

    }

    private void logout(){
        new AsyncTask<Void, Void, Result>() {
            @Override
            protected Result doInBackground(Void... voids) {
                Result result = new Result();

                XPSApiEndpoint xps = XPSClient.getXPSClient(ThongTinTaiKhoanActivity.this);
                Call<String> call = xps.logout(new XPSEmptyParam());
                String response = null;
                try {
                    response = call.execute().body();

                    JSONObject root = new JSONObject(response);
                    JSONObject resultObj = root.getJSONObject("result");

                    String username = resultObj.getString("username");
                    result.setUsername(username);

                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                return result;
            }

            @Override
            protected void onPostExecute(Result s) {
                super.onPostExecute(s);
                if (s.getUsername() == null)
                    Toast.makeText(ThongTinTaiKhoanActivity.this,
                            "log out thanh cong",
                            Toast.LENGTH_SHORT).show();
                textView.setText("");
                SharedPrefManager.getInstance(ThongTinTaiKhoanActivity.this).clear();
                finish();
            }
        }.execute();

    }

    private void anhxa(){
        textView    =findViewById(R.id.textView);
        button      =findViewById(R.id.button);
        button2     =findViewById(R.id.button2);
        button3     =findViewById(R.id.button3);
        imageView   =findViewById(R.id.imageView);
    }

    private void loadImg() {

        new AsyncTask<Void, Void, String>() {

            @Override
            protected String doInBackground(Void... voids) {

                int[] arrInt = new int[]{907};
                String[] arrStr = new String[]{"name", "login", "image_small"};

                ArrayList args = new ArrayList();
                args.add(arrInt);
                args.add(arrStr);

                String method = "read";
                String model = "res.users";

                XPSApiEndpoint xps = XPSClient.getXPSClient(ThongTinTaiKhoanActivity.this);
                XPSExecParam param = new XPSExecParam(model, method, args, new Object());
                Call<String> call = xps.loadImg(param);
                String response = null;
                String s = null;
                try {
                    response = call.execute().body();

                    JSONObject root = new JSONObject(response);
                    JSONArray resultArr = root.getJSONArray("result");
                    JSONObject resultObj = resultArr.getJSONObject(0);

                    s = resultObj.getString("image_small");


                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                return s;
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                Bitmap bitmap = convertStringToBitMap(s);

                imageView.setImageBitmap(bitmap);
            }
        }.execute();

    }

    private Bitmap convertStringToBitMap(String encodedString) {
        try {
            byte[] encodeByte = Base64.decode(encodedString, Base64.DEFAULT);
            Bitmap bitmap = BitmapFactory.decodeByteArray(encodeByte, 0, encodeByte.length);
            return bitmap;
        } catch (Exception e) {
            e.getMessage();
            return null;
        }
    }
}
