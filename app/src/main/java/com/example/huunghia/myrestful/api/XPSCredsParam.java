package com.example.huunghia.myrestful.api;

public class XPSCredsParam {

    public XPSCredsParamDetail params;

    public XPSCredsParam(String db, String login, String password) {
        this.params = new XPSCredsParamDetail(db, login, password);
    }

    public static class XPSCredsParamDetail {
        public String db;
        public String login;
        public String password;

        public XPSCredsParamDetail(String db, String login, String password) {
            this.db = db;
            this.login = login;
            this.password = password;
        }
    }
}
