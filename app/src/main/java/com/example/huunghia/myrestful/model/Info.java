package com.example.huunghia.myrestful.model;

public class Info {
    private String mobile;
    private String email;
    private int id;
    private String phone;
    private String name;

    public Info(String mobile, String email, int id, String phone, String name) {
        this.mobile = mobile;
        this.email = email;
        this.id = id;
        this.phone = phone;
        this.name = name;
    }

    @Override
    public String toString() {
        return "mobile: " + mobile +"\n"
                +"email: " + email +"\n"
                +"id: " + id +"\n"
                +"phone: " + phone +"\n"
                +"name: " + name;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
